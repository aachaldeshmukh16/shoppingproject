package com.thoughtworks.service;

import java.util.List;

import com.thoughtworks.model.Product;

public interface Shop {
	public List<Product> addProduct();
	public List<Product> getProducts(List<Product> li);
	public List<Product> removeProduct(List<Product> li);
	public List<Product> getProductsBrandWise(List<Product> li, String brand);
	public List<Product> addProductToCart(List<Product> li,int cartpid);
	public void getCartProducts(List<Product> cartlist);
	public List<Product> removeCartProduct(List<Product> cartList);
}
