package com.thoughtworks.admin;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.thoughtworks.model.Product;
import com.thoughtworks.service.Shop;
import com.thoughtworks.serviceImpl.shopOperations;

public class Application {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		List<Product> li = new ArrayList();
		List<Product> cartlist = new ArrayList<>();
		System.out.println("please enter username: ");
		String uname = sc.next();
		System.out.println("please enter password: ");
		String pass = sc.next();
		Shop shop = new shopOperations();

		Product p = new Product();
		p.setPid(1);
		p.setPname("Jeans");
		p.setPcat("Cloths");
		p.setPbrand("Puma");
		p.setPprice(1000);

		Product p1 = new Product();
		p1.setPid(2);
		p1.setPname("Shirts");
		p1.setPcat("Cloths");
		p1.setPbrand("Zara");
		p1.setPprice(800);

		Product p2 = new Product();
		p2.setPid(3);
		p2.setPname("T-shirt");
		p2.setPcat("Cloths");
		p2.setPbrand("Zara");
		p2.setPprice(300);

		Product p3 = new Product();
		p3.setPid(4);
		p3.setPname("Bags");
		p3.setPcat("Accessories");
		p3.setPbrand("Puma");
		p3.setPprice(900);

		Product p4 = new Product();
		p4.setPid(5);
		p4.setPname("Rings");
		p4.setPcat("Accessories");
		p4.setPbrand("Peora");
		p4.setPprice(1300);

		Product p5 = new Product();
		p5.setPid(6);
		p5.setPname("Sandles");
		p5.setPcat("Accessories");
		p5.setPbrand("Adidas");
		p5.setPprice(900);

		li.add(p);
		li.add(p1);
		li.add(p2);
		li.add(p3);
		li.add(p4);
		li.add(p5);

		if(uname.equals("admin") && pass.equals("admin"))
		{
			while(true) {
				System.out.println("enter your choice : \n1.Add Product\n2.View Products\n3.Remove Product\n4. To View product brand wise ");
				int choice = sc.nextInt();

				switch (choice) {
					case 1:
						li = shop.addProduct();
						break;
					case 2:
						shop.getProducts(li);
						break;

					case 3:
						li = shop.removeProduct(li);
						break;

					case 4:
						System.out.println("Enter brand name: ");
						String brand = sc.next();
						shop.getProductsBrandWise(li,brand);
					default:
						System.out.println("enter valid choice");
						break;
				}
			}
		}
		else
		{

			while(true)
			{
				System.out.println("enter your choice : \n1.View Products\n2.View Product brandwise\n3. Add product to cart\n4. View cart products\n5. Remove product from cart");
				int choice = sc.nextInt();

				switch (choice) {
					case 1:
						shop.getProducts(li);
						break;

					case 2:
						System.out.println("Enter brand name: ");
						String brand = sc.next();
						shop.getProductsBrandWise(li,brand);
						break;

					case 3:
						System.out.println("enter id of product which you want to add to cart");
						int cartpid = sc.nextInt();
						cartlist = shop.addProductToCart(li, cartpid);
						break;

					case 4:
						shop.getCartProducts(cartlist);
						break;

					case 5:
						shop.removeCartProduct(cartlist);
						break;

					default:
						System.out.println("enter valid choice");
						break;
				}
			}

		}
	}

}
