package com.thoughtworks.serviceImpl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import com.thoughtworks.model.Product;
import com.thoughtworks.service.Shop;

public class shopOperations implements Shop{
	Scanner sc = new Scanner(System.in);
	List<Product> li = new ArrayList<>();
	List<Product> cartlist = new ArrayList<>();
	@Override
	public List<Product> addProduct() {

		System.out.println("how many products you want to add: ");
		int noOfProducts = sc.nextInt();

		for(int i = 0;i<noOfProducts;i++)
		{
			Product pro = new Product();
			System.out.println("please enter product id: ");
			int pid = sc.nextInt();
			pro.setPid(pid);
			System.out.println("please enter product name: ");
			String pname = sc.next();
			pro.setPname(pname);
			System.out.println("please enter product category: ");
			String pcat = sc.next();
			pro.setPcat(pcat);
			System.out.println("enter product price : ");
			float pprice = sc.nextFloat();
			pro.setPprice(pprice);

			System.out.println("please enter product brand: ");
			String pbrand = sc.next();
			pro.setPbrand(pbrand);

			li.add(pro);
			System.out.println("product added successfully");

		}
		return li ;
	}
	@Override
	public List<Product> getProducts(List<Product> li1) {
		for(Product p:li1)
		{
			System.out.println("Product id: "+p.getPid());
			System.out.println("Product name: "+p.getPname());
			System.out.println("Product category: "+p.getPcat());
			System.out.println("Price : "+p.getPprice());
			System.out.println("Product brand : "+p.getPbrand());
			System.out.println();
		}
		return null;
	}
	@Override
	public List<Product> removeProduct(List<Product> li1) {
		System.out.println("enter the product id which product you want to remove : ");
		int proid = sc.nextInt();

		Iterator<Product> itr = li1.iterator();
		while (itr.hasNext()) {
			Product p = itr.next();
			if(proid == p.getPid())
			{
				li1.remove(p);
				System.out.println("Product removed successfully");
				break;
			}
		}

		return li1;

	}
	@Override
	public List<Product> getProductsBrandWise(List<Product> li,String brand) {
		for(Product p :li)
		{
			if(p.getPbrand().equals(brand))
			{
				System.out.println("Product id: "+p.getPid());
				System.out.println("Product name: "+p.getPname());
				System.out.println("Product category: "+p.getPcat());
				System.out.println("Price : "+p.getPprice());
				System.out.println("Product brand : "+p.getPbrand());
				System.out.println();
			}
		}
		return null;
	}
	@Override
	public List<Product> addProductToCart(List<Product> li,int cartpid) {
		for(Product p:li)
		{
			if(p.getPid() == cartpid)
			{
				cartlist.add(p);
				System.out.println("product added successfully");
			}
		}
		return cartlist;
	}
	@Override
	public void getCartProducts(List<Product> cartlist) {

			for (Product p : cartlist) {
				System.out.println("Product id: " + p.getPid());
				System.out.println("Product name: " + p.getPname());
				System.out.println("Product category: " + p.getPcat());
				System.out.println("Price : " + p.getPprice());
				System.out.println("Product brand : " + p.getPbrand());
				System.out.println();

		}

	}
	@Override
	public List<Product> removeCartProduct(List<Product> cartList) {
		System.out.println("enter the product id which product you want to remove : ");
		int proid = sc.nextInt();

		Iterator<Product> itr = cartList.iterator();
		while (itr.hasNext()) {
			Product p = itr.next();
			if(proid == p.getPid())
			{
				cartList.remove(p);
				System.out.println("Product removed successfully");
				System.out.println("");
				break;
			}
		}

		return cartList;
	}


}
